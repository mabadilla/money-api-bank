# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'money/bank/version'

Gem::Specification.new do |spec|
  spec.name          = "money-api-bank"
  spec.version       = Money::Bank::VERSION
  spec.authors       = ["MJ Abadilla"]
  spec.email         = ["mjmaix@gmail.com"]

  spec.summary       = "A gem that can be used with custom Exchange Rate API. Ideal for own apps using money gem"
  spec.description   = "A gem that provides rates for the money gem that will use provided URL. Modified clone of money-historical-bank gem."
  spec.homepage      = "https://github.com/mjmaix/#{spec.name}"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'https://rubygems.org'
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.14"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_dependency "yajl-ruby", ">=0.8.3"
  spec.add_dependency "money", ">=3.7.1"
  spec.add_development_dependency "minitest", ">=2.0"
  spec.add_development_dependency "rr", ">=1.0.4"
end