# encoding: UTF-8
require 'money'
require 'date'
require 'yajl'
require 'open-uri'

class Money
  module Bank
    # will store returned exchange rate
    module ExchangeRatesLoader
      API_URL = ENV['RATE_URL']

      def load_data(date, from, to)
        rates_source = API_URL + "?on=#{date.strftime('%Y-%m-%d')}&from=#{from}&to=#{to}"
        doc = Yajl::Parser.parse(open(rates_source).read)

        internal_set_rate(date, doc.from, doc.to, doc.rate) unless doc.blank?
      end
    end
  end
end
